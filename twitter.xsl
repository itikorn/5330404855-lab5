<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>twittwer.xsl</title>
            </head>
            <body>
                <h1>
                <a>
                        <xsl:attribute name="href"> 
                            <xsl:value-of select="rss/channel/link"/> 
                        </xsl:attribute>
                        <xsl:value-of select="rss/channel/link"/> 
                </a>
                </h1>
                <table border="1">
                    <tr>
                        <th width="300">Title</th>
                        <th width="30">Publication Date</th>
                    </tr>
                    <xsl:for-each select="rss/channel/item">
                    <tr>
                         <td>
                         <a>
                         <xsl:attribute name="href"> 
                                <xsl:value-of select="link"/> 
                         </xsl:attribute> <!--a href="link"-->
                         <xsl:value-of select="title"/> 
                         </a>
                         </td>
                         <td>
                                <xsl:value-of select="pubDate"/> 
                         </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
