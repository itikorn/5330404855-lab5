<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:lib="http://www.zvon.org/library">
    
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <body>
            <head>
                <title>books.xsl</title>
            </head>
                
               <table border="1">
                   <tr>
                       <th>TiTle</th>
                       <th>Pages</th>
                   </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                    <xsl:sort select="lib:pages" order="ascending" data-type="number"/>
                            <xsl:if test="lib:pages"><!--use namespace lib:pages only-->
                    <tr>
                           <td>          
                                   <xsl:value-of select="@about"/>
                           </td>
                       <td><xsl:value-of select="lib:pages"/></td>
                    </tr>  
                           </xsl:if>
                    </xsl:for-each>
               </table>
                  
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
