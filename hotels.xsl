<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <h1>
                        <IMG>
                        <xsl:attribute name="src">
                        <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href" />
                        </xsl:attribute>  <!--img src="link"-->
                        </IMG>
                            <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                            
                </h1>
                            <xsl:for-each select="kml:kml/kml:Document">
                            <xsl:text>List of hotels</xsl:text>
                            <ul>
                                <li><xsl:value-of select="kml:Placemark/kml:name"/></li>
                                <ul><!--ul same tr-->
                                    <li>
                                        <xsl:param name="url" select="kml:Placemark/kml:description"/>
                                        <a>                                             
                                             <xsl:attribute name="href">                                               
                                                      <xsl:value-of select="substring-before(substring-after($url,'&#34;'),'&#34;')"/>
                                             
                                              </xsl:attribute>            
                                                      <xsl:value-of select="substring-before(substring-after($url,'&#34;'),'&#34;')"/>
                                        </a>
                                        
                                    </li>
                                </ul>
                                <ul> 
                                    <li><xsl:value-of select="kml:Placemark/kml:Point/kml:coordinates"/></li>
                                </ul>
                                </ul>
                                
                                
                            <!----> 
                            <ul>
                                <li><xsl:value-of select="kml:Placemark[2]/kml:name"/></li>
                                <ul><!--ul same tr-->
                                    <li>
                                        <xsl:param name="url2" select="kml:Placemark[2]/kml:description"/>
                                        <a>                                             
                                             <xsl:attribute name="href">                                               
                                                      <xsl:value-of select="substring-before(substring-after($url2,'&#34;'),'&#34;')"/>
                                             
                                              </xsl:attribute>            
                                                      <xsl:value-of select="substring-before(substring-after($url2,'&#34;'),'&#34;')"/>
                                        </a>
                                        
                                    </li>
                                </ul>
                                <ul> 
                                    <li><xsl:value-of select="kml:Placemark[2]/kml:Point/kml:coordinates"/></li>
                                </ul>
                                </ul>
                           <!----> 
                            
                            <ul>
                                <li><xsl:value-of select="kml:Placemark[3]/kml:name"/></li>
                                <ul><!--ul same tr-->
                                    <li>
                                        <xsl:param name="url3" select="kml:Placemark[3]/kml:description"/>
                                        <a>                                             
                                             <xsl:attribute name="href">                                               
                                                      <xsl:value-of select="substring-before(substring-after($url3,'&#34;'),'&#34;')"/>
                                             
                                              </xsl:attribute>            
                                                      <xsl:value-of select="substring-before(substring-after($url3,'&#34;'),'&#34;')"/>
                                        </a>
                                        
                                    </li>
                                </ul>
                                <ul> 
                                    <li><xsl:value-of select="kml:Placemark[3]/kml:Point/kml:coordinates"/></li>
                                </ul>
                                </ul>
                            <!----> 
                            
                            <ul>
                                <li><xsl:value-of select="kml:Placemark[4]/kml:name"/></li>
                                <ul><!--ul same tr-->
                                    <li>
                                        <xsl:param name="url4" select="kml:Placemark[4]/kml:description"/>
                                        <a>                                             
                                             <xsl:attribute name="href">                                               
                                                      <xsl:value-of select="substring-before(substring-after($url4,'&#34;'),'&#34;')"/>
                                             
                                              </xsl:attribute>            
                                                      <xsl:value-of select="substring-before(substring-after($url4,'&#34;'),'&#34;')"/>
                                        </a>
                                        
                                    </li>
                                </ul>
                                <ul> 
                                    <li><xsl:value-of select="kml:Placemark[4]/kml:Point/kml:coordinates"/></li>
                                </ul>
                                </ul>
                            
                            </xsl:for-each>
                
               
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>